package com.shopov.cardsapp.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopov.cardsapp.R;
import com.shopov.cardsapp.model.Organisation;

public class OrganisationsAdapter extends BaseAdapter {
	
	private List<Organisation> data = null;
	private Organisation tempValues = null;
	private LayoutInflater inflater = null;
	private Context context = null;

	public OrganisationsAdapter(Context context, List<Organisation> data) {
		this.data = data;
		this.context = context;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	private static class ViewHolder {
		public ImageView organisationImage;
		public TextView organisationName;
		public TextView organisationAddress;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		ViewHolder viewHolder;
		
		if (convertView == null) {
			vi = inflater.inflate(com.shopov.cardsapp.R.layout.list_row, null);
			viewHolder = new ViewHolder();
			viewHolder.organisationImage = (ImageView) vi.findViewById(com.shopov.cardsapp.R.id.list_image);
			viewHolder.organisationName = (TextView) vi.findViewById(com.shopov.cardsapp.R.id.organisation_name);
			viewHolder.organisationAddress = (TextView) vi.findViewById(com.shopov.cardsapp.R.id.organisation_address);
			
			vi.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) vi.getTag();
		}
		
		if (data.size() <= 0) {
			
		} else {
			tempValues = (Organisation) data.get(position);
			viewHolder.organisationImage.setImageResource(R.drawable.coffee_img);
			viewHolder.organisationName.setText(tempValues.getName());
			viewHolder.organisationAddress.setText(tempValues.getAddress());
		}
		
		return vi;
	}

}
