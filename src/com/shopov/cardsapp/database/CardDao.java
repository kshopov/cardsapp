package com.shopov.cardsapp.database;

import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.shopov.cardsapp.database.CardTable.CardColumns;
import com.shopov.cardsapp.model.Card;

public class CardDao implements Dao<Card> {
	
	private static final String INSERT = 
			"INSERT INTO " + CardTable.TABLE_NAME + "("
				+ CardColumns.FIRST_NAME + ", "
				+ CardColumns.LAST_NAME + ", "
				+ CardColumns.EXPIRY + ", "
				+ CardColumns.CLUB_NAME + ", "
				+ CardColumns.CARD_NUMBER + ", "
				+ CardColumns.SAVINGS + ")"
					+ "values (?, ?, ?, ?, ?, ?)";
	
	private SQLiteDatabase db = null;
	private SQLiteStatement insertStatement = null;
	
	public CardDao(SQLiteDatabase db) {
		this.db = db;
		insertStatement = db.compileStatement(INSERT);
	}

	@Override
	public long save(Card card) {
		insertStatement.clearBindings();
		insertStatement.bindString(1, card.getFirstName());
		insertStatement.bindString(2, card.getLastName());
		insertStatement.bindString(3, card.getExpiry());
		insertStatement.bindString(4, card.getClubName());
		insertStatement.bindString(5, card.getCardNumber());
		insertStatement.bindDouble(6, card.getSavings());
		
		return insertStatement.executeInsert();
	}

	@Override
	public long update(Card type) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long delete(Card type) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Card get(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Card> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
