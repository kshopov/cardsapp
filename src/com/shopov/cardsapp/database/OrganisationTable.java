package com.shopov.cardsapp.database;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class OrganisationTable {
	
	public static final String TABLE_NAME = "organisations";
	
	public static class CardColumns implements BaseColumns {
		public static final String FIRST_NAME = "first_name";
		public static final String LAST_NAME = "last_name";
		public static final String EXPIRY = "expiry";
		public static final String CLUB_NAME = "club_name";
		public static final String CARD_NUMBER = "card_number";
		public static final String SAVINGS = "savings";
	}
	
	public static void onCreate(SQLiteDatabase db) {
		
	}

}
