package com.shopov.cardsapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Organisation implements Parcelable {

	private long id;
	private String name;
	private String address;
	private String email;
	private String webPage;
	private String phoneNumber;
	private boolean isFavorite;
	private int discount;
	private double gpsLongtitude;
	private double gpsLatitude;

	public Organisation() {
		id = 0;
		name = "";
		address = "";
		email = "";
		phoneNumber = "";
		isFavorite = false;
		discount = 0;
		gpsLatitude = 0.0;
		gpsLongtitude = 0.0;
	}

	public Organisation(Parcel in) {
		readFromParcel(in);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebPage() {
		return webPage;
	}

	public void setWebPage(String webPage) {
		this.webPage = webPage;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isFavorite() {
		return isFavorite;
	}

	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public double getGpsLongtitude() {
		return gpsLongtitude;
	}

	public void setGpsLongtitude(double gpsLongtitude) {
		this.gpsLongtitude = gpsLongtitude;
	}

	public double getGpsLatitude() {
		return gpsLatitude;
	}

	public void setGpsLatitude(double gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeLong(id);
		out.writeString(name);
		out.writeString(address);
		out.writeString(email);
		out.writeString(webPage);
		out.writeString(phoneNumber);
		out.writeByte((byte) (isFavorite ? 1 : 0));  //if myBoolean == true, byte == 1
		out.writeInt(discount);
		out.writeDouble(gpsLatitude);
		out.writeDouble(gpsLongtitude);
	}
	
	private void readFromParcel(Parcel in) {
		id = in.readLong();
		name = in.readString();
		address = in.readString();
		email =in.readString();
		webPage = in.readString();
		phoneNumber = in.readString();
		isFavorite = in.readByte() != 0;     //myBoolean == true if byte != 0

		discount = in.readInt();
		gpsLatitude = in.readDouble();
		gpsLongtitude = in.readDouble();
	}
	
	public static final Parcelable.Creator<Card> CREATOR = new Parcelable.Creator<Card>() {
		public Card createFromParcel(Parcel in) {
			return new Card(in);
		}

		public Card[] newArray(int size) {
			return new Card[size];
		}
	};

}
