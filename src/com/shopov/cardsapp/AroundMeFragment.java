package com.shopov.cardsapp;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class AroundMeFragment extends Fragment implements LocationListener {
	private static final String KEY_CONTENT = "Around Me";
	private String mContent = "TEST FRAGMENT";
	
	private double gpsLongtitude;
	private double gpsLatitude;
	private String provider;
	private LocationManager locationManager;
	private Location location;

	private MapView mapView;
	private GoogleMap map;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if ((savedInstanceState != null)
				&& savedInstanceState.containsKey(KEY_CONTENT)) {
			mContent = savedInstanceState.getString(KEY_CONTENT);
		}
		
		locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		Criteria c = new Criteria();
		provider = locationManager.getBestProvider(c, false);
        location = locationManager.getLastKnownLocation(provider);
        if (location != null)
        {
        	gpsLongtitude = location.getLongitude();
            gpsLatitude = location.getLatitude();
        }
				
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.aroundme_fragment_layout, null);
		mapView = (MapView) v.findViewById(R.id.map_view);
		mapView.onCreate(savedInstanceState);

		// Gets to GoogleMap from the MapView and does initialization stuff
		map = mapView.getMap();
		map.getUiSettings().setMyLocationButtonEnabled(false);
		map.setMyLocationEnabled(true);

		MapsInitializer.initialize(this.getActivity());

		MarkerOptions marker = new MarkerOptions().position(new LatLng(gpsLatitude, gpsLongtitude)).title("Blok 36");
		MarkerOptions marker1 = new MarkerOptions().position(new LatLng(42.138654, 24.783788)).title("Blok 34");
		MarkerOptions marker2 = new MarkerOptions().position(new LatLng(42.141724, 24.786316)).title("Blok 22");
		map.addMarker(marker);
		map.addMarker(marker1);
		map.addMarker(marker2);
		// Updates the location and zoom of the MapView
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
				new LatLng(gpsLatitude, gpsLongtitude ), 15);
		map.animateCamera(cameraUpdate);

		return v;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(KEY_CONTENT, mContent);
	}

	@Override
	public void onResume() {
		mapView.onResume();
		super.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapView.onLowMemory();
	}

	@Override
	public void onLocationChanged(Location location) {
		gpsLongtitude = location.getLongitude();
        gpsLatitude = location.getLatitude();
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

}
