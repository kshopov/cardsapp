package com.shopov.cardsapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class SavingsFragment extends Fragment {
	private static final String KEY_CONTENT = "Savings fragment";
	private String mContent = "TEST FRAGMENT";
	
	Button calculateDiscountButton = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
    }

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View v = inflater.inflate(R.layout.savings_fragment_layout, null);
    	final TextView totalSavingsTextView = (TextView) v.findViewById(R.id.savings_total_textview);
    	final TextView currentSavingsTextView = (TextView) v.findViewById(R.id.current_savings_textview);
    	
    	totalSavingsTextView.setText(getText(R.string.total_savings) + " " + String.valueOf(23.47) + getText(R.string.leva));
    	calculateDiscountButton = (Button) v.findViewById(R.id.calculate_discount_button);
    	calculateDiscountButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				currentSavingsTextView.setText("TEXT");
			}
		});
    	
        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }
    
}
