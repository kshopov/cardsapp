package com.shopov.cardsapp;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.shopov.cardsapp.adapters.OrganisationsAdapter;
import com.shopov.cardsapp.model.Organisation;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

public class OrganisationsFragment extends Fragment {
	private static final String KEY_CONTENT = "TestFragment:Content";
	private String mContent = "TEST FRAGMENT";
	
	private String[] cities = {
			"������",
			"�������",
			"�����",
			"�����",
			"������"
	};
	
	private String[] branches = {
			"������",
			"������",
			"����������",
			"������"
	};
	
	private Organisation org1 = new Organisation();
	private Organisation org2 = new Organisation();
	
	private ProgressDialog pd = null;
	
	List<Organisation> organisations;
	
	ListView listView = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
        
        organisations = new ArrayList<Organisation>();
        
        org1.setName("Broadway cafe");
        org1.setAddress("Trakia bl 52");
        
        org2.setName("������� �����");
        org2.setAddress("������ �� 142 �� \"�\"");
        
        organisations.add(org1);
        organisations.add(org2);
    }

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View v = inflater.inflate(R.layout.organisations_fragment_layout, container, false);
    	listView = (ListView) v.findViewById(R.id.list);
    	final Spinner citiesSpinner = (Spinner) v.findViewById(R.id.cities_spinner);
    	ArrayAdapter<String> citiesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, cities);
    	citiesSpinner.setAdapter(citiesAdapter);
    	
    	final Spinner branchesSpinner = (Spinner) v.findViewById(R.id.branches_spinner);
    	ArrayAdapter<String> branchesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, branches);
    	branchesSpinner.setAdapter(branchesAdapter);
    	OrganisationsAdapter adapter = new OrganisationsAdapter(getActivity(), organisations);
    	listView.setAdapter(adapter);
    	
    	final Button searchButton = (Button) v.findViewById(R.id.search_button);
    	searchButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				pd = ProgressDialog.show(getActivity(), getText(R.string.searching), "");
				SendData data = new SendData();
				data.execute("www.abv.bg");
			}
		});
    	
    	
        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }
    
    private class SendData extends AsyncTask<String, Void, JSONObject> {

		@Override
		protected JSONObject doInBackground(String... params) {
			return null;
		}
		
		@Override
		protected void onPostExecute(JSONObject result) {
			super.onPostExecute(result);
			pd.hide();
		}
    	
    }
}
